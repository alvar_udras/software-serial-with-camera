#define ARDUINO_MAIN

// Disable some warnings for the Arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wattributes"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wuninitialized"

#include <Arduino.h>

/*
* Libraries
*/
// Standard Arduino source files for serial:
#include <HardwareSerial.h>
#include <SoftWareSerial.h>

// Other source files, depends on your program which you need
#include <Print.h>
#include <New.h>
#include <wiring_digital.c>
#include <wiring_analog.c>
#include <WString.h>
#include <WMath.cpp>
#include <Stream.h>
#include "libraries/Adafruit_VC0706.h"


// Unused source files:
//#include <WInterrupts.c>
//#include <wiring_pulse.c>
//#include <wiring_shift.c>
//#include <IPAddress.cpp>
//#include <Tone.cpp>

// Restore original warnings configuration
#pragma GCC diagnostic pop

SoftwareSerial softSerial(10, 11); // RX, TX

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  while (!Serial) {
	  ; // wait for serial port to connect. Needed for native USB port only
  }
  // set the data rate for the SoftwareSerial port
  softSerial.begin(19200);
}



/*
* Main code called on reset; the sketch harness
*/

int main(void)
{
	init();
	
	setup();
		
	Adafruit_VC0706 vc0706(&Serial);
	//vc0706.begin(115200);
	uint8_t bytesRead = 0;
	char* res = vc0706.getVersion(bytesRead);
	//for ( int i = 0 ; i < bytesRead ; i++ ) {
		//softSerial.write(res[i]);
	//}
	
	vc0706.takePicture();
	uint32_t picLen = vc0706.frameLength();
	// 100 is max buffer leng
	while ( picLen > 100 ) {
		uint8_t* res = vc0706.readPicture(100);
		for ( int i = 0 ; i < 100 ; i++ ) {
			softSerial.write(res[i]);
		}
		picLen -= 100;
	}
	
	if (picLen > 0 ) {
		uint8_t* res = vc0706.readPicture(100);
		for ( int i = 0 ; i < 100 ; i++ ) {
			softSerial.write(res[i]);
		}
	}
	
	vc0706.resumeVideo();
	
	return 0;
}