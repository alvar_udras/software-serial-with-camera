import serial
from time import sleep

def printHex(inputData):
    print( " ".join([ '%02x'%c for c in inputData] ) )

class RequestFailedError(Exception):
    def __init__(self, value):
        self.value = value
        
    def __str__(self):
        return repr(self.value)

def checkForError(cmdResult):
    if (cmdResult[3]== 0x3):
        raise RequestFailedError("Error!")

def executeCmd(camera, cmd):
    camera.write(cmd)
    resultBuf = bytearray()
    resultBuf.append(camera.read(1))
    while camera.inWaiting() > 0:
        resultBuf.append(camera.read(1))
    checkForError(resultBuf)
    return resultBuf

def equalBitmaps(bitmap1, bitmap2):
    if ( len(bitmap1) != len(bitmap2)):
        return False;
    for i in range(0, len(bitmap1)):
        if ( bitmap1[i] != bitmap2[i]):
            return False        
    return True
        
    
def takePicture(camera):
    stopFrameCmd = [0x56, 0, 0x36, 0x1, 0]
    executeCmd(camera, stopFrameCmd)

    getFbufLenCmd = [0x56, 0, 0x34, 0x1, 0]
    resultBuf = executeCmd(camera, getFbufLenCmd)

    dataLen =[resultBuf[5], resultBuf[6], resultBuf[7], resultBuf[8]]
    print("Length of data returned")
    printHex(dataLen)


    readFBufCmd = [0x56, 0, 0x32, 0xC, 0, 0xA, 0,0,0,0]
    readFBufCmd+= dataLen
    readFBufCmd+= [0x10, 0]
    
    executeCmd(camera, readFBufCmd)
    
    #remove data begin and data end bitmasks before writing to file!
    
    dataEndBitmask = [0x76, 0, 0x32, 0, 0]
    imgBuf=bytearray()
    imgBuf+=camera.read(10)
    while equalBitmaps(imgBuf[len(imgBuf)-len(dataEndBitmask):len(imgBuf)], dataEndBitmask) == False:        
        imgBuf.append(camera.read(1))
        
    f = open('img.jpg', 'wb')
    #remove the bytes, that indicate end of data
    imgBuf = imgBuf[0:len(imgBuf)-len(dataEndBitmask)]
    print("length of file %02x"%len(imgBuf))
    f.write(imgBuf)    
    f.close()
    
    resumeFrameCmd = [0x56, 0, 0x36, 0x1, 0x3]
    
    executeCmd(camera, resumeFrameCmd)

def readConfig(camera):
    readConfigCmd = [0x56, 0, 0x30, 0x6, 0x6, 0x1, 0x0, 0x0, 0x0, 0x7]
    result = executeCmd(camera, readConfigCmd)
    printHex(result)
    

if __name__ == "__main__":
    camera = serial.Serial()
    camera.baudrate = 115200
    camera.port = 15
    camera.timeout = 5
    camera.open()
    action = ''
    while action != 'q':
        try:
            action = raw_input('select action:')
            if ( action == 't'):
                takePicture(camera)
            if ( action == 'c'):
                readConfig(camera)
        except RequestFailedError:
            print ("failed to make request!")
        